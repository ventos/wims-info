Pour le moment, la plateforme d'exercices interactifs pour `WIMS
<http://wims.unice.fr/>`_ contient très peu de chose pour
l'enseignement de l'informatique. L'objectif de ce dépôt est de
développer collaborativement de nouveaux modules.

Modules:

- `<U2~coding~oefprogramC.fr/>`_: un dérivé du module éponyme déjà
  existant dans WIMS, avec quelques menues améliorations.

- `Compréhension de programmes C++ <test~coding~readingCppPrograms.fr/>`_:
  deviner l'entrée ou la sortie d'un programme
