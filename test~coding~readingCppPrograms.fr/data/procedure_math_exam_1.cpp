#include <iostream>

using namespace std;

void Pascal(int &acc, int n) {
    acc = 0;
    for (int i = n; i > 0; i--)
        acc += i;
    return acc;
}

int main() {
    int somme;

    Pascal(somme, 8);
    cout << "La somme est de : " << somme << endl;

    return 0;
}
