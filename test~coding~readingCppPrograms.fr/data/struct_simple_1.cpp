#include <iostream>
#include <cmath>

using namespace std;

struct Point2D {
    double x;
    double y;
};

int main() {
    Point2D p1 = {-2, -3};
    Point2D p2 = {-4,  4};
    
    double dx = fabs(p1.x - p2.x);
    double dy = fabs(p1.y - p2.y);
    
    cout << dx << " " << dy << endl;
    
    return 0;
}