#include <iostream>

using namespace std;

struct cerise {
    int queue;
    int gauche;
    int droite;
};

struct cerisier {
    cerise cerise_gauche;
    int cime;
    cerise cerise_droite;
};

void initialise_cerise(cerise &miam, int val) {
    miam.queue = val;
    miam.gauche = val - 1;
    miam.droite = val + 1;
}

void initialise_cerisier(cerisier &tree, int val) {
    tree.cime = val;
    initialise_cerise(tree.cerise_gauche, val - 2);
    initialise_cerise(tree.cerise_droite, val + 2);
}

int somme_cerise(cerise miam) {
    return miam.queue + miam.gauche + miam.droite;
}

int main() {
    int n;
    cerisier tree;
    cin >> n;
    initialise_cerisier(tree, n);
    cout << tree.cime + somme_cerise(tree.cerise_gauche) + somme_cerise(tree.cerise_droite) << endl;
    return 0;
}
