#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<vector <int>> tab = {{1, 2, 3}, {4, 5, 6}};

    for (int i = 0; i < tab.size(); i++) {
        int s = 0;
        for (int j = 0; j < tab[i].size(); j++) {
            s = s + tab[i][j];
        }
        cout << s << endl;
    }
}
