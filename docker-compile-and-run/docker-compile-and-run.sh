#!/bin/sh

# TODO:
# - set tight resources limits
# - make this into a program rather than a shell script for a minimum of safety

usage () {
    echo docker-compile-and-run [program.cpp]
    echo
    echo Compiles and executes a c++ program within a sandbox.
    echo The standard input and output is passed down to the program.
    echo The exit status is that of the compiler.
}

if [ x"$1" = "x" ]; then
    usage
    exit 0
fi

ID=`sudo docker run -d crosbymichael/build-essential sleep 1000`
sudo docker exec -i $ID sh -c "cat > prog.cpp" < $1

if sudo docker exec $ID g++ prog.cpp; then
   sudo docker exec -i $ID ./a.out
else
   exit $?
fi
sudo docker rm -f $ID > /dev/null 2>&1
